/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionpapergenerator;

import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 *
 * @author user
 */
public class SetUIDetails {
    void setDataInCombobox(JComboBox cmb, ArrayList arrList) {
       Iterator<String> iter = arrList.iterator();
       
        while (iter.hasNext()) {
            cmb.addItem(iter.next());
            cmb.validate();
            cmb.revalidate();
            iter.remove();
        }  
    }
    
     void addingMarksInCombobox(JComboBox marks_cb) {
        marks_cb.addItem("20");
        marks_cb.addItem("80");
    }
     
     void addingQuestionMarksInCombobox(JComboBox marks_cb) {
        marks_cb.addItem("5");
        marks_cb.addItem("10");
    }
     
     void addingStartingItemInCombobox(JComboBox branch, JComboBox semester,JComboBox subject, JComboBox mode, JComboBox marks){
        branch.addItem("Choose Branch: ");
        semester.addItem("Choose Sem: ");
        subject.addItem("Choose Subject");
        marks.addItem("Choose Marks: ");
        mode.addItem("Choose Difficulty Level");
        branch.setSelectedItem("Choose Branch");
        marks.setSelectedItem("Choose Marks: ");
        semester.setSelectedItem("Choose Sem");
        subject.setSelectedItem("Choose Subject");        
        mode.setSelectedItem("Choose Difficulty Level");
    }
     
     void addItemsInPanel(JPanel chaptersPanel, ArrayList chaptersList, ArrayList selectedChapters) {
         chaptersPanel.removeAll();
        chaptersPanel.setLayout(new GridLayout(5, 5));
        chaptersPanel.setVisible(true);
        JCheckBox[] box = new JCheckBox[chaptersList.size()];
        int chaptersCount = chaptersList.size();
        for(int i=0; i<chaptersList.size(); i++) {
            System.out.println(chaptersList.get(i));
            box[i] = new JCheckBox((String) chaptersList.get(i));
            chaptersPanel.add(box[i]);
            box[i].addItemListener(new ItemListener(){
                @Override
                public void itemStateChanged(ItemEvent e) {
                    for(int i = 0; i<box.length; i++){
                        if(box[i].isSelected()){
                            if(! selectedChapters.contains(box[i].getText())){
                                selectedChapters.add(box[i].getText());
                                System.out.println("Selected: " + selectedChapters);
                            }
                        }
                        if(! box[i].isSelected()) {
                            if(selectedChapters.contains(box[i].getText())){
                                selectedChapters.remove(box[i].getText());
                                System.out.println("Selected: " + selectedChapters);
                            }
                        }
                    }
                }

            });

            chaptersPanel.validate();
            }
            chaptersPanel.validate();
            chaptersPanel.revalidate();
            chaptersPanel.repaint();
     }
}
