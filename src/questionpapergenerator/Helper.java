/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionpapergenerator;

import static java.lang.Math.ceil;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;

/**
 *
 * @author user
 */
public class Helper {
    ArrayList<Integer> getSeggregationOfChapters(ArrayList<Integer> arrChps) {
        int len = (int) ceil((double)arrChps.size()/2);
        System.out.println("Len : " + len);
        ArrayList<Integer> chapters = new ArrayList<>();
        System.out.println("Chp seggregation: " + arrChps);
         int j;
         if(len>1){
            for(j=0; j<(len-1); j++) {
               chapters.add(arrChps.get(j));
               System.out.println(arrChps.get(j));
            }
            for(int i=0; i<arrChps.size(); i++) {
                for(j=0; j<chapters.size(); j++){
                   if(chapters.get(j) == (arrChps.get(i))) {
                        arrChps.remove(i);
                   }
                }
            }
         }
         else {
             chapters.add(arrChps.get(0));
             arrChps.remove(0);
         }
         return chapters;
   }
   
    String integerArrayListToString(ArrayList<Integer> arrList) {
       String inString = "";
        for(int i =0; i<arrList.size(); i++)
        {
            if(i==0)
                inString = inString + "'" +arrList.get(i) + "'";
            else
                inString = inString + ",'" + arrList.get(i) + "'";
        }
        return inString;
    }
   
    String stringArrayListToString(ArrayList<String> arrList) {
       String inString = "";
        for(int i =0; i<arrList.size(); i++)
        {
            if(i==0)
                inString = inString + "'" +arrList.get(i) + "'";
            else
                inString = inString + ",'" + arrList.get(i) + "'";
        }
        return inString;
   }
    
    ArrayList insertDataInArrayList(ResultSet rs, String col) {
        ArrayList arrlist = new ArrayList<>();
        try {
            while(rs.next()) {
                arrlist.add(rs.getString(col));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrlist;
    }
    
    ArrayList insertModeInArrayList(ResultSet rs, String col) {
        ArrayList arrlist = new ArrayList<>();
        try {
            while(rs.next()) {
                if("0".equals(rs.getString(col))){
                    arrlist.add("Easy");
                } else if("1".equals(rs.getString(col))) {
                    arrlist.add("Medium");
                } else if("2".equals(rs.getString(col))) {
                    arrlist.add("Difficult");
                }
            } 
        } catch (SQLException ex) {
            Logger.getLogger(Helper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arrlist;
    }
    
    
    int  getMode(String mode){
      System.out.println("MODE" +mode);
      int modeLevel = 0;
      if("Easy".equals(mode)) {
          modeLevel = 0;
      } else if("Medium".equals(mode)) {
          modeLevel = 1;
      } else if("Difficult".equals(mode)) {
          modeLevel = 2;
      }
      return modeLevel;
    }

}
