/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionpapergenerator;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class DBClass {
    Connection conn;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Helper helper = new Helper();
    
    public DBClass(){
        conn = MySQLConnect.getConnection();
    }
    
    ArrayList getBranchName(){
        try {
            String branch = "Select branch_name from Branch";
            ps = conn.prepareStatement(branch);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList arr = helper.insertDataInArrayList(rs, "branch_name");
        return arr;
    }
    
    ArrayList getSemester(){
        try {
            String semester = "SELECT sem FROM Semester";
            ps = conn.prepareStatement(semester);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList arr = helper.insertDataInArrayList(rs, "sem");
        return arr;
    }
     
    ArrayList getModeOfDifficulty(){
        try {
            String mode = "SELECT DISTINCT(Mode_of_difficulty) from question";
            ps = conn.prepareStatement(mode);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList arr = helper.insertModeInArrayList(rs, "mode_of_difficulty");
        return arr;
    }
    
    ArrayList getChaptersName(String subject){
        try {
            String chapterSql = "SELECT chapter_name FROM chapter where subject_id = (SELECT subject_id from subject where subject_name  = '"+subject+"' )";
            ps = conn.prepareStatement(chapterSql);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList arr = helper.insertDataInArrayList(rs, "chapter_name");
        return arr;
    }
     
    ArrayList getSubjectsName(String branch, String semester){
        try {
            String subjectSql = "SELECT subject_name from subject where subject_id in (SELECT branch_subject_semester.subject_id FROM (( branch_subject_semester INNER JOIN branch ON branch_subject_semester.branch_id = branch.branch_id AND branch.branch_name= '"+branch+"' ) INNER JOIN semester ON branch_subject_semester.semester_id = semester.semester_id AND semester.sem = '"+semester+"' ))";
            ps = conn.prepareStatement(subjectSql);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList arr = helper.insertDataInArrayList(rs, "subject_name");
        return arr;
    }
    
    int minFive(String arrChpsString, int modeLevel) {
        int minFive = 0;
        try {
            String fiveMin = "SELECT min(question_id) as min FROM question WHERE chapter_id IN ("+ arrChpsString +") AND marks = 5 AND deleted_at = 0 AND mode_of_difficulty = "+ modeLevel;
                
            PreparedStatement pstem = conn.prepareStatement(fiveMin);
            ResultSet rstem = pstem.executeQuery();
            while(rstem.next()){

                minFive = rstem.getInt("min");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return minFive;           
    }
    
    int maxFive(String arrChpsString, int modeLevel) {
        int maxFive = 0;
        try {
            String fiveMax = "SELECT max(question_id) as max FROM question WHERE chapter_id IN ("+ arrChpsString +") AND marks = 5 AND deleted_at = 0 AND mode_of_difficulty = "+ modeLevel;
                
            PreparedStatement pstem = conn.prepareStatement(fiveMax);
            ResultSet rstem = pstem.executeQuery();
            while(rstem.next()){
                maxFive = rstem.getInt("max");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maxFive;    
    }
    
    int minTen(String arrChpsString, int modeLevel) {
         int minTen = 0;
        try {
            String tenMin = "SELECT min(question_id) as min FROM question WHERE chapter_id IN ("+ arrChpsString +") AND marks = 10 AND mode_of_difficulty = "+ modeLevel;
                
            PreparedStatement pstem = conn.prepareStatement(tenMin);
            ResultSet rstem = pstem.executeQuery();
            while(rstem.next()){
                minTen = rstem.getInt("min");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return minTen;    
    }
    
    int maxTen(String arrChpsString, int modeLevel) {
         int maxTen = 0;
        try {
            String tenMax = "SELECT max(question_id) as max FROM question WHERE chapter_id IN ("+ arrChpsString +") AND marks = 10 AND deleted_at = 0 AND mode_of_difficulty = "+ modeLevel;
                
            PreparedStatement pstem = conn.prepareStatement(tenMax);
            ResultSet rstem = pstem.executeQuery();
            while(rstem.next()){
                maxTen = rstem.getInt("max");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return maxTen;    
    }
    
  
    int getProbability(){
        int maxProbability = 0, minProbability = 0,probability = 0;
        try {
            String minMaxProbability = "SELECT max(probability) as max, min(probability) as min FROM `question`";
            PreparedStatement ps1 = conn.prepareStatement(minMaxProbability);
            ResultSet rs1 = ps1.executeQuery();
            while(rs1.next()) {
                maxProbability = rs1.getInt("max");
                minProbability = rs1.getInt("min");
            }
            if(minProbability == maxProbability){
                probability = maxProbability;
            } else if((maxProbability-minProbability) > 0) {
                probability = (maxProbability-minProbability);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return probability;
    }
    
    public ResultSet getQuestionResultSet(int id, ArrayList<Integer> arrChps, int max, int min, int mode, int probability, int marks){
        ResultSet resultS = null;
        System.out.println("In method!");
        String chapters = helper.integerArrayListToString(arrChps);
        try {
           String getQuestion = "SELECT * FROM question WHERE question_id = RAND()*(("+max+" + "+ min +")-"+min+") AND deleted_at = 0 AND chapter_id IN ("+chapters+") AND mode_of_difficulty = "+mode +" AND probability < " + probability + " AND marks = "+marks;
            System.out.println(getQuestion);       
            PreparedStatement prepareS = conn.prepareStatement(getQuestion);
            resultS = prepareS.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultS;
    }
    
    ArrayList sortChaptersThroughWeight(String inString) {
        ResultSet result = null;
        try {
            String chapterWeightSql = "SELECT chapter_id FROM chapter WHERE chapter_name IN ("+ inString +") ORDER BY weight";
            PreparedStatement prepare = conn.prepareStatement(chapterWeightSql);
            result = prepare.executeQuery();
            System.out.println("Result set of sort arrayChps " + result);
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArrayList arrChps = helper.insertDataInArrayList(result, "chapter_id");
        return arrChps;
    }
    
    int getChapterId(String chapter) {
        int chapterId = 0;
        
        String getChpterSql = "SELECT chapter_id from chapter where chapter_name = '" + chapter + "'";
        try {
            PreparedStatement ps1 = conn.prepareStatement(getChpterSql);
            ResultSet rs1 = ps1.executeQuery();
            while(rs1.next()) {
                chapterId = rs1.getInt("chapter_id");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NewJFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return chapterId;
    }
    
    void incrementProbability(int id){
        try {
            String updateProbability = "UPDATE question SET probability = probability+1 WHERE question_id =" + id;
            PreparedStatement p = conn.prepareStatement(updateProbability);
            p.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DBClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
